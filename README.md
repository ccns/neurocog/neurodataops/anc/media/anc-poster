# ANC Poster

Source of the poster presenting [Austrian NeuroCloud](https://ccns.gitlab.io/neurocog/neurodataops/anc/media/anc-website) project. The poster was presented at [Unlocking the Value of Knowledge: ERA Symposium 2023](https://www.ffg.at/veranstaltung/era-symposium-2023) and can serve as a general-purpose poster.

## Poster

### SVG file

![ANC Poster SVG](https://gitlab.com/ccns/neurocog/neurodataops/anc/media/anc-poster/-/jobs/artifacts/main/raw/anc_poster_plain.svg?job=build-poster-files)

### PDF file

![ANC Poster PDF](https://gitlab.com/ccns/neurocog/neurodataops/anc/media/anc-poster/-/jobs/artifacts/main/raw/anc_poster.pdf?job=build-poster-files)


## Fonts

The poster uses two fonts:
- [Montserrat](https://fonts.google.com/specimen/Montserrat) for project name under the logo
- [Open Sans](https://fonts.google.com/specimen/Open+Sans) for all other texts

## Colors

The blue ANC Logo color is, as specified in [ANC Logo](https://gitlab.com/ccns/neurocog/neurodataops/anc/media/anc-logo):

| Color    | RGB           | CMYK          | Hex       |
| :------- | :------------ | :------------ | :-------- |
| ANC blue | `0,81,142`    | `100,43,0,44` | `#00518e` |

## Working with source file

The poster is designed with [Inkscape](https://inkscape.org/), a cross-platform advanced open-source vector graphics software. The standard output files are Inkscape-specific SVG files. Thus, Inkscape is needed to edit the files but you can view them with other tools.

To work with the source files, [install Inkscape](https://inkscape.org/release/). On Debian use `apt install inkscape`.

You also need to have the fonts installed on your system to see the text correctly. You can download the fonts from Google Fonts links or follow the [instructions on GitHub](https://github.com/google/fonts).

## License

The content of the poster and it's design are subject to [CC BY license](https://creativecommons.org/licenses/by/4.0/).
